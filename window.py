import sys
from adminHelper import create
from adminHelper import update
from adminHelper import readUser
from adminHelper import readAll
from adminHelper import delete
from adminHelper import create_exam_personnel
from adminHelper import delete_exam_personnel
from adminHelper import update_exam_personnel
from subjectHelper import createSubject
from subjectHelper import readSubject
from lecturerHelper import createQuestionAns
from lecturerHelper import updateQA
from lecturerHelper import deleteQA
from lecturerHelper import readQA
from lecturerHelper import update_credentials
from examHelper import readExamPaper
from examHelper import createExamPaper
from examHelper import editExamPaper
from admin import login_lecturer
from admin import login_examPersonnel

def switch(window):
    match window:
        case 1:
            i  = 3
            for _ in range(i):
                username = input("Enter admin username: ")
                password = input("Enter admin password: ")
                while username == "admin" and password == "password":
                    print(f"{"\033[92m"}1. Create new lecturer\n2. Update lecturer\n3. Show lecturer details\n4. Show all lecturers\n5. Delete lecturers and details\n6. Create new exam-personnel\n7. Delete exam-personnel\n8. Create new subject (min 3 subject with 3 each topic)\n9. Back to main window " + "\033[0m")
                    operation = input("Enter operation Number: ")
                    try: 
                        admin_task= int(operation)
                        match admin_task:
                            case 1:
                                create()
                            case 2:
                                update()
                            case 3:
                                readUser()
                            case 4: 
                                readAll()  
                            case 5: 
                                delete()
                            case 6:
                                create_exam_personnel()
                            case 7:
                                delete_exam_personnel()
                            case 8: 
                                createSubject()
                            case 9:
                                main()
                            case _:
                                print('Invalid choice')  
                    except ValueError:
                        print("Please enter the choice in number!!")
                     
                else:
                    if i != 1:
                        print("Invalid Credentials!! Try again.")
                    i = i - 1
                if i == 0:
                    print("Program Terminated!!!") 
                
        case 2:
            j = 3
            for _ in range(j):
                username = input("Enter lecturer username: ")
                password = input("Enter lecturer password: ")
                result = login_lecturer('lecturer.txt',username, password)
                while result == True:
                    print(f"{"\033[92m"}1. Create question answer\n2. Update question answer\n3. Delete a question\n4. Read QnA from subject & topic\n5. Update username and password\n6. Back to main window" + "\033[0m")
                    operation = input("Enter operation Number: ")
                    try:
                        lecturer_task = int(operation)
                        match lecturer_task:
                            case 1:
                                createQuestionAns()   
                            case 2:
                                updateQA()
                            case 3:
                                deleteQA() 
                            case 4:
                                readQA()
                            case 5:
                                update_credentials()
                            case 6:
                                main()
                            case _:
                                print('Invalid choice. Please enter valid number.') 
                    except ValueError:
                        print("Please enter the choice in number!!")
                    
                else:
                    if j != 1:
                        print("Invalid Credentials!! Try again.")
                    j = j - 1
                if j == 0:
                    print("Program Terminated!!!")    

        case 3:
            k = 3
            for _ in range(k):
                username = input("Enter exam personnel username: ")
                password = input("Enter exam personnel password: ")
                result = login_examPersonnel('exam_personnel.txt',username, password)
                while result == True:
                    print(f"{"\033[92m"}1. Create exam questions \n2. Read exam questions\n3. Edit exam paper\n4. Delete exam personnel\n5. Update exam personnel\n6. Back to main window" + "\033[0m")
                    operation = input("Enter operation Number: ")
                    try:
                        examPersonnel_task = int(operation)
                        match examPersonnel_task:
                            case 1:
                                createExamPaper()
                            case 2:
                                readExamPaper()
                            case 3:
                                editExamPaper()
                            case 4:
                                delete_exam_personnel()
                            case 5:
                                update_exam_personnel()
                            case 6:
                                main()
                            case _:
                                print('Invalid choice. Please enter valid number.') 
                    except ValueError:
                        print("Please enter the choice in number!!")
                else:
                    if k != 1:
                        print("Invalid Credentials!! Try again.")
                    k = k - 1
                if k == 0:
                    print("Program Terminated!!!") 

        case 4:
            print("Program Terminatted")
            sys.exit()

        case _:
            print(f"{"\033[91m"}Please enter valid choice and try again!!!" + "\033[0m")
            main()

def main():
    print(f"{"\033[1m"}<|--------------------------------- Team 20 -----------------------------------|>" + "\033[0m")
    print(f"{"\033[1m"}<|-------- Shekhar Sunuwar| Kiran Pandey | Rajaki aryal | Abhit Mahato --------|>" + "\033[0m")
    print(f"{"\033[1m"}<|-------- NP069601       | NP0698268    | NP069633     | NP069692  -----------|>" + "\033[0m")
    print(f"{"\033[1m"}<|------------------ Select the operation to perform: -------------------------|>" + "\033[0m")
    print(f"{"\033[92m"}1. Admin window\n2. Lecturer window\n3. Exam-Personnel window\n4. Exit" + "\033[0m")
    choice = input("Enter operation number to perform: ")
    try:
        window = int(choice)
        switch(window)
    except ValueError:
        print("Enter the choice in number!!")
main()
