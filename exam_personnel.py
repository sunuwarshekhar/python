from pathlib import Path

def create_mcq_question(filename,question, answer):
    results_dir = Path("results")
    filename = results_dir / filename
    results_dir.mkdir(parents=True, exist_ok=True)
    with open(filename, 'a') as file:
        file.write(f"{question},{answer}\n")        
                      
def create_subjective_question(filename,question):
    results_dir = Path("results")
    filename = results_dir / filename
    results_dir.mkdir(parents=True, exist_ok=True)
    with open(filename, 'a') as file:
        file.write(f"{question},\n")

def check_QA(filename, question):
    results_dir = Path("results")
    filename = results_dir / filename
    results_dir.mkdir(parents=True, exist_ok=True)
    with open(filename, 'r') as file:           
        for line in file:
            fields = line.strip().split(',')
            if fields[0] == question and fields[1] != "":
                return f"{fields[0],{fields[1]}}"
            elif fields[0] == question:
                return f"{fields[0]}"
            
def update_question(filename, question, updated_question, updated_answer):
    results_dir = Path("results")
    filename = results_dir / filename
    temp_question = []
    results_dir.mkdir(parents=True, exist_ok=True)
    with open(filename, 'r') as file:
        for line in file:
            fields = line.strip().split(',')
            if fields[0] == question:
                temp_question.append(f"{updated_question},{updated_answer}\n")
            else:
                temp_question.append(line)
    with open(filename, 'w') as file:
        file.writelines(temp_question)

def read_QA(filename):
    results_dir = Path("results")
    filename = results_dir / filename
    results_dir.mkdir(parents=True, exist_ok=True)
    with open(filename, 'r') as file:           
        for line in file:
            fields = line.strip().split(',')
            if fields[1]:
                print(f"{fields[0]} {fields[1]}")
            else:
                print(fields[0]) 