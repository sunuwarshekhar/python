from pathlib import Path
from validation import validateNumber, validateString        
                
def accept_input():
    subject = {} 
    inputSubject = input("Enter Subject Name: ")
    finalSubject = validateString(inputSubject, 3, "subject")
    subject["name"] = finalSubject
    InputSubjectTopicNo = input("Enter Number of topics to add: ")
    subjectTopicNo = validateNumber(InputSubjectTopicNo, 1, "subject topic number")
    subject["total_topic"] = subjectTopicNo
    total =[]
    for x in range(subject["total_topic"]):
         topic = input(f"{x+1}. topic name: ")
         total.append(topic)
         subject["topic_names"] = total
    return subject

def create_subject(filename, subject):
    results_dir = Path("results")
    filename = results_dir / filename
    results_dir.mkdir(parents=True, exist_ok=True)
    with open(filename, 'a') as file:
        file.write(f"{subject["name"]},{subject["topic_names"]}\n")

def read_subject(filename, subject):
    results_dir = Path("results")
    filename = results_dir / filename
    results_dir.mkdir(parents=True, exist_ok=True)
    with open(filename, 'r') as file:
        for line in file:
            fields = line.strip().split(',')
            if fields[0] == subject:
                subjectCount = len(fields) 
                n = 0
                result = []
                for x in range(subjectCount - 1):
                    n= n + 1
                    result.append(fields[n])
                    new = [s.strip().strip("'") for s in result]
                return f"{fields[0]}|{new}"

