from pathlib import Path
from validation import validateCharacter
from validation import validateFullName
from validation import validateNumber
from validation import validateDate
from validation import validateEmail
from validation import validateString

def input_new_user():
    user = {}  

    usernameInput = input("Enter new lecturer username: ")
    username = validateCharacter(usernameInput, 4, "username")
    user['username'] = username

    passwordInput = input("Enter password: ")
    password = validateCharacter(passwordInput, 6, "password")
    user["password"] = password

    fullnameInput = input("Enter full name: ")
    fullname = validateFullName(fullnameInput, 6)
    user["full_name"] = fullname

    ageInput = input("Enter age: ")
    age = validateNumber(ageInput, 1, "age")
    user["age"] = age

    dateInput = input("Enter date of birth (YYYY-MM-DD): ")
    date = validateDate(dateInput)
    user["dob"] = date

    addressInput = input("Enter address: ")
    address = validateString(addressInput, 3, "address")
    user["address"] = address

    contactInput = input("Enter contact number: ")
    contact = validateNumber(contactInput, 10, "contact no.")
    user["contact_no"] = contact

    inputEmail = input("Enter email: ")
    email = validateEmail(inputEmail)
    user["email"] = email

    citizenInput = input("Enter citizenship: ")
    citizen = validateNumber(citizenInput, 4, "citizen")
    user["citizen"] = citizen 
    
    return user

def create_user(filename, user):
    results_dir = Path("results")
    filename = results_dir / filename
    results_dir.mkdir(parents=True, exist_ok=True)
    with open(filename, 'a') as file:
        file.write(f"{user["username"]},{user["password"]},{user["full_name"]},{user["age"]},{user["dob"]},{user["address"]},{user["contact_no"]},{user["email"]},{user["citizen"]}\n")

def check_user(filename, username):
        results_dir = Path("results")
        filename = results_dir / filename
        results_dir.mkdir(parents=True, exist_ok=True)
        with open(filename, 'r') as file:
            isFound = False
            for line in file:
                fields = line.strip().split(',')
                if fields[0] == username:
                    isFound = True
                    return
                else:
                    isFound = False
            print(isFound,'isFound')        
        return isFound  

def read_user(filename, username):
        results_dir = Path("results")
        filename = results_dir / filename
        results_dir.mkdir(parents=True, exist_ok=True)
        with open(filename, 'r') as file:
            for line in file:
                fields = line.strip().split(',')
                if fields[0] == username:
                    return f"Username:{fields[0]} FullName:{fields[2]} Age:{int(fields[3])}"

def read_users(filename):
        results_dir = Path("results")
        filename = results_dir / filename
        results_dir.mkdir(parents=True, exist_ok=True)
        with open(filename, 'r') as file:
            for line in file:
                fields = line.strip()
                print(fields)
            try:     
                if(fields):
                    print()
                else:
                    return None
            except NameError:
                print("no user found")    

def updated_details():
    user = {}  

    usernameInput = input("Enter new username: ")
    username = validateCharacter(usernameInput, 4, "username")
    user['username'] = username

    fullnameInput = input("Enter full name: ")
    fullname = validateFullName(fullnameInput, 6)
    user["full_name"] = fullname

    ageInput = input("Enter age: ")
    age = validateNumber(ageInput, 1, "age")
    user["age"] = age

    dateInput = input("Enter date of birth (YYYY-MM-DD): ")
    date = validateDate(dateInput)
    user["dob"] = date

    addressInput = input("Enter address: ")
    address = validateString(addressInput, 3, "address")
    user["address"] = address

    contactInput = input("Enter contact number: ")
    contact = validateNumber(contactInput, 10, "contact no.")
    user["contact_no"] = contact

    inputEmail = input("Enter email: ")
    email = validateEmail(inputEmail)
    user["email"] = email

    citizenInput = input("Enter citizenship: ")
    citizen = validateNumber(citizenInput, 4, "citizen")
    user["citizen"] = citizen 

    return user     

def update_user(filename, username, updated_user):
        users = []
        results_dir = Path("results")
        filename = results_dir / filename
        results_dir.mkdir(parents=True, exist_ok=True)
        with open(filename, 'r') as file:
            for line in file:
                fields = line.strip().split(',')
                if fields[0] == username:
                    users.append(f"{updated_user["username"]},{fields[1]},{updated_user["full_name"]},{updated_user["age"]},{updated_user["dob"]},{updated_user["address"]},{updated_user["contact_no"]},{updated_user["email"]},{updated_user["citizen"]}\n")
                else:
                    users.append(line)
        with open(filename, 'w') as file:
            file.writelines(users)   

def delete_user(filename, username):
        users = []
        results_dir = Path("results")
        filename = results_dir / filename
        results_dir.mkdir(parents=True, exist_ok=True)
        with open(filename, 'r') as file:
            for line in file:
                fields = line.strip().split(',')
                if fields[0] != username:
                    users.append(line)
        with open(filename, 'w') as file:
            file.writelines(users)

def update_lecturer(filename,oldUsername, newUsername, password):
        users = []
        results_dir = Path("results")
        filename = results_dir / filename
        results_dir.mkdir(parents=True, exist_ok=True)
        with open(filename, 'r') as file:
            for line in file:
                fields = line.strip().split(',')
                if fields[0] == oldUsername:
                    users.append(f"{newUsername},{password},{fields[2]},{fields[3]},{fields[4]},{fields[5]},{fields[6]},{fields[7]},{fields[8]}\n")
                else:
                    users.append(line)
        with open(filename, 'w') as file:
            file.writelines(users)

#exam personnel
def create_examPersonnel(filename, username, password, valid_email):
        results_dir = Path("results")
        filename = results_dir / filename
        results_dir.mkdir(parents=True, exist_ok=True)
        with open(filename, 'a') as file:
            file.write(f"{username},{password},{valid_email}\n")

def check_examPersonnel(filename, username):
        results_dir = Path("results")
        filename = results_dir / filename
        with open(filename, 'r') as file:
            isFound = False
            for line in file:
                fields = line.strip().split(',')
                if fields[0] == username:
                    isFound = True
                    return
                else:
                    isFound = False
            print(isFound,'isFound')        
        return isFound  

def delete_examPersonnel(filename, username):
        users = []
        results_dir = Path("results")
        filename = results_dir / filename
        results_dir.mkdir(parents=True, exist_ok=True)
        with open(filename, 'r') as file:
            for line in file:
                fields = line.strip().split(',')
                if fields[0] != username:
                    users.append(line)
        with open(filename, 'w') as file:
            file.writelines(users)

def update_examPersonnel_details(filename,oldUsername, newUsername, password):
        users = []
        results_dir = Path("results")
        filename = results_dir / filename
        with open(filename, 'r') as file:
            for line in file:
                fields = line.strip().split(',')
                if fields[0] == oldUsername:
                    users.append(f"{newUsername},{password}\n")
                else:
                    users.append(line)
        with open(filename, 'w') as file:
            file.writelines(users) 

def login_lecturer(filename, username, password):
        results_dir = Path("results")
        filename = results_dir / filename
        results_dir.mkdir(parents=True, exist_ok=True)
        with open(filename, 'r') as file:
            for line in file:
                fields = line.strip().split(',')
                if fields[0] == username and fields[1] == password:
                    isFound = True
                    return isFound
                else:
                    isFound = False
        return isFound 

def login_examPersonnel(filename, username, password):
        results_dir = Path("results")
        filename = results_dir / filename
        with open(filename, 'r') as file:
            for line in file:
                fields = line.strip().split(',')
                if fields[0] == username and fields[1] == password:
                    isFound = True
                    return isFound
                else:
                    isFound = False
        return isFound 
