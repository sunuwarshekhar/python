from pathlib import Path

def accept_input():
    qa = {}  
    qa["question"] = input("Enter Question: ")
    qa["answer"] = input("Enter answer: ")
    return qa

def create_QA(filename, data, subj,qa):
    results_dir = Path("results")
    filename = results_dir / filename
    results_dir.mkdir(parents=True, exist_ok=True)
    with open(filename, 'a') as file:
        file.write(f"{data},{subj},{qa["question"]},{qa["answer"]}\n")

def update_QA(filename, subject):
    results_dir = Path("results")
    filename = results_dir / filename
    results_dir.mkdir(parents=True, exist_ok=True)
    with open(filename, 'r') as file:
        isFound = False
        result = []
        for line in file:
            fields = line.split(',')
            if fields[0] == subject:
                topic = input("Enter topic of the question:")
                if fields[1] == topic:
                    question = input('Enter updated question:')
                    anwser = input('Enter updated answer:')
                    isFound = True
                    result.append(f"{fields[0]},{fields[1]},{question},{anwser}\n")
                if fields[1] != topic:
                    result.append(line)
            else: 
                result.append(line)  
    with open(filename, 'w') as file:
        file.writelines(result)
    return isFound 

def check_QA(filename, question):
    results_dir = Path("results")
    filename = results_dir / filename
    results_dir.mkdir(parents=True, exist_ok=True)
    with open(filename, 'r') as file:
        isFound = False
        for line in file:
            fields = line.strip().split(',')
            if fields[2] == question:
                isFound = True
                return
            else:
                isFound = False
    return isFound 

def delete_QA(filename, question):
    results_dir = Path("results")
    filename = results_dir / filename
    users = []
    results_dir.mkdir(parents=True, exist_ok=True)
    with open(filename, 'r') as file:
        for line in file:
            fields = line.strip().split(',')
            if fields[2] != question:
                users.append(line)
    with open(filename, 'w') as file:
        file.writelines(users)

def read_QA(filename, subject, topic):
    results_dir = Path("results")
    filename = results_dir / filename
    results_dir.mkdir(parents=True, exist_ok=True)
    with open(filename, 'r') as file:
        isFound = False
        for line in file:
            fields = line.strip().split(',')
            if (fields[0] == subject and fields[1] == topic):
                print("Question: ",fields[2])
                print("Answer: ",fields[3])
            else:
                isFound = False
    return isFound 

def return_qa(filename, question):
    results_dir = Path("results")
    filename = results_dir / filename
    results_dir.mkdir(parents=True, exist_ok=True)
    with open(filename, 'r') as file:
        isFound = False
        for line in file:
            fields = line.strip().split(',')
            if (fields[2] == question):
                return f"{fields[2]},{fields[3]}"
            else:
                isFound = False
    return isFound 

def return_question(filename, question):
    results_dir = Path("results")
    filename = results_dir / filename
    results_dir.mkdir(parents=True, exist_ok=True)
    with open(filename, 'r') as file:
        isFound = False
        for line in file:
            fields = line.strip().split(',')
            if (fields[2] == question):
                return fields[2]
            else:
                isFound = False
    return isFound 