from subject import accept_input
from subject import create_subject
from subject import read_subject
from validation import validateCharacter, validateNumber

def createSubject():
    InputSubjectNumber = input("Enter the number of subject to add: ")
    subjectNumber = validateNumber(InputSubjectNumber, 1, "subject number")
    for _ in range(subjectNumber):
        res = accept_input()
        create_subject("subject.txt",res)
        print("Subject created successfully and stored in subject.txt file")

def readSubject():
    InputSubject =  input("Enter subject: ")
    subject = validateCharacter(InputSubject, 4, "subject")   
    res = read_subject("subject.txt",subject)
    fields = res.strip().split('|')
    result = fields[1]
    ans = result.strip("[]").replace('"',"").strip("[]").replace("'","")
    data = ans.strip().split(',')
    n = 0
    for x in data:
        n = n + 1
        print(f"{n}. {x.strip()}")

