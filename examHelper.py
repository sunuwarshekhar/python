from lecturer import return_question
from lecturer import return_qa
from exam_personnel import create_mcq_question
from exam_personnel import create_subjective_question
from exam_personnel import check_QA
from exam_personnel import update_question
from exam_personnel import read_QA
from validation import validateNumber

set_A = "SET_A.txt"
set_B = "SET_B.txt"

def createExamPaper():
     filename = ''
     res = input("Enter set A or B: ")
     if res == "A":
         filename = set_A
     elif res == "B":
         filename = set_B
     else:
         print("Incorrect input")
         return
     mcqInput = input("Enter number of MCQ to add (min 5): ")
     mcq = validateNumber(mcqInput, 1, "MCQ")

     subjectiveInput = input("Enter number of Subjective question to add (min 3): ")
     subjective = validateNumber(subjectiveInput, 1, "subjective question")
     
     if(mcq >= 5 and subjective >= 3) :     
          for _ in range(mcq):
               res = input("Enter MCQ question (question part only): ")
               result = return_qa('questionAnswer.txt',res)
               question = result.split(',')[0]
               answer = result.split(',')[1]
               create_mcq_question(filename,question,answer)
          for _ in range(subjective):
               res = input("Enter subjective question: ")
               question = return_question('questionAnswer.txt',res)
               create_subjective_question(filename,question)
          print(f"Questions Added to set {filename} successfully")
     else:
          print(f"Minimum question criteria not meet (5 MCQ & 3 Subjective)")
          print(f"You entered {mcq} MCQ and {subjective} subjective question.")
    

def editExamPaper():
    res = input("Enter set A or B: ")
    if res == "A":
         filename = set_A
    elif res == "B":
         filename = set_B
    else:
         print("Incorrect input")
         return
    question = input("Enter the question which you want to edit: ")
    res = check_QA(filename,question)
    if len(res.split(',')) > 1 :
         #MCQ question
         updated_question = input("Enter correction question: ")
         updated_answer = input("Enter correction answer: ")
         update_question(filename,question, updated_question, updated_answer)
    else:
         #subjective question
         updated_question = input("Enter correction question: ")
         updated_answer = ''
         update_question(filename,question, updated_question, updated_answer)

def readExamPaper():
        res = input("Enter set A or B to show question: ")
        if res == "A":
               filename = set_A
               read_QA(filename)
        elif res == "B":
               filename = set_B
               read_QA(filename)
        else:
               print("Please enter correct input")
               return
        

