from admin import input_new_user
from admin import create_user
from admin import check_user
from admin import read_user
from admin import read_users
from admin import updated_details
from admin import update_user
from admin import delete_user
from admin import create_examPersonnel
from admin import check_examPersonnel
from admin import delete_examPersonnel
from admin import update_examPersonnel_details
import os

from validation import validateCharacter, validateEmail

# Create an instance of Person
def create():
    result = input_new_user()
    create_user("lecturer.txt",result)
    print("Lecturer created successfully and stored in lecturer.txt file")
# Read all user
def readAll():
    results_folder = "results"
    file_path = os.path.join(results_folder, "lecturer.txt")
    if (os.path.isfile(file_path)):

        read_users("lecturer.txt")
    else:
        print("file not found.")
# # Read a user
def readUser():
    username = input("Enter lecturer username: ")
    try:
        found_user = read_user("lecturer.txt",username)
        if found_user:
            print("<--User found-->\n", found_user)
        else:
            print("User not found.")
    except ValueError:
        print("User not found.")
    # if found_user:
    # else:

# Update a user
def update():
    username = input("Enter lecturer username: ")
    res = check_user("lecturer.txt",username)
    if res == False:
        print('user not found')
        return
    else:
        updated_user = updated_details()
        update_user("lecturer.txt",username, updated_user)
        print("Lecutrer updated successfully and stored in lecturer.txt file")

# delete a user
def delete(): 
    username = input('Enter Lecturer Username: ')
    res = check_user("lecturer.txt",username)
    if res == False:
        print("No user with this username")
        return
    else:
        delete_user("lecturer.txt",username)
        print('user deleted successfully')

#exam personnel
def check_username(username):
        while len(username) < 3:
            username = input("Enter username (must be greater than 4 characters): ")
        return username

def check_password(password):
        while len(password) < 5:
            password = input("Enter password (must be greater than 6 characters): ")
        return password


def create_exam_personnel(): 
    usernameInput = input("Enter Exam Personnel Username: ")
    final_username = validateCharacter(usernameInput, 4, "username")
    passwordInput = input("Set password: ")
    final_password = validateCharacter(passwordInput, 6, "password")
    emailInput = input("Enter Email: ")
    valid_email = validateEmail(emailInput)
    create_examPersonnel("exam_personnel.txt",final_username, final_password, valid_email)
    print("Exam personnel created successfully and stored in exam_personnel.txt")

def delete_exam_personnel(): 
    username = input('Enter username to delete: ')
    res = check_examPersonnel("exam_personnel.txt",username)
    if res == False:
        print("No user with this username")
        return
    else:
        delete_examPersonnel("exam_personnel.txt",username)
        print('user deleted successfully')

def update_exam_personnel():
    oldUsername = input("Enter exam personnel username: ")
    res = check_examPersonnel("exam_personnel.txt",oldUsername)
    if res == False:
        print('user not found')
        return
    else:
        usernameInput = input("Set new username: ")
        newUsername = validateCharacter(usernameInput, 4, "username")
        passwordInput = input("Set new password: ")
        password = validateCharacter(passwordInput, 6, "password")
        update_examPersonnel_details("exam_personnel.txt",oldUsername, newUsername, password)
        print("Exam personnel updated successfully and stored in exam_personnel.txt")
          