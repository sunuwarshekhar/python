def validateString(value, length, type):
    notation = True
    while notation == True:
        if value.isalpha():
            if len(value) >= length:
                notation = False
                return value
            else:
                print(f"Enter min {length} character for {type}")
                value = input(f"Enter {type} in correct format: ")
        else:
            print(f'{type} contains only letters')
            value = input(f"Enter {type} in correct format: ")



def validateFullName(value, length):
    notation = True
    while notation == True:
        if all(part.isalpha() for part in value.split()):
            if len(value) >= length:
                notation = False
                return value
            else:
                print(f"Enter at least {length} characters for the full name.")
                value = input("Enter full name in correct format: ") 
        else:
            print("Full name must contain only letters and spaces.")
            value = input("Enter full name in correct format: ")


def validateCharacter(value, length, type):
    notation = True
    while notation == True:
        if len(value) >= length:
            notation = False
            return value
        else:
            print(f"Enter min {length} character for {type}")
            value = input(f"Enter {type} in correct format: ")
            
def validateEmail(email):
    flag=True
    while flag==True:
        if email.endswith('@gmail.com') or email.endswith('@hotmail.com') or email.endswith('@yahoo.com'):
            flag=False
            return email
        else:
            print("Invalid Email!!")
            email = input("Enter your email in correct format: ")
    
def validateNumber(value, length, type):
    notation=True
    while notation == True:
        if value.isdigit():
            if len(value) >= length:
                notation = False
                return int(value)
            else:
                print(f'{type} length must be {length}')
                value = input(f"Enter your {type} with length {length}: ")
        else:
            print(f'{type} contains only number')
            value = input(f"Enter your {type} in correct format: ")


from datetime import datetime

def validateDate(value):
    notation=True
    while notation == True:
        try:
            # Attempt to parse the date
            valid_date = datetime.strptime(value, "%Y-%m-%d")
            return valid_date.strftime("%Y-%m-%d")
        except ValueError:
            print(f"Date must be in the format yyyy-mm-dd.")
            value = input(f"Enter date in correct format: ")

