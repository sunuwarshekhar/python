from admin import update_lecturer
from subjectHelper import read_subject
from lecturer import accept_input
from lecturer import create_QA
from lecturer import update_QA
from lecturer import check_QA
from lecturer import delete_QA
from lecturer import read_QA
from validation import validateString,validateCharacter

def createQuestionAns():
    dataInput = input('Enter the subject name: ')
    data = validateString(dataInput, 3, "subject")
    ans = read_subject('subject.txt',data)
    if(ans):
        subj = input('Enter the topic: ')
    fields = ans.strip().split('|')
    result = fields[1]
    ans2 = result.strip("[]").replace('"',"").strip("[]").replace("'","")
    data2 = ans2.strip().split(',')
    for x in data2:
        if(x.strip() == subj):
            queAns = accept_input()
            create_QA("questionAnswer.txt",data, subj, queAns)
            print('Question Ans added successfully and stored in questionAnswer.txt')

def updateQA():
    subject = input("Enter Subject of question:")
    res = update_QA("questionAnswer.txt",subject)
    if(res == True):
        print("Question updated Successfully and stored in questionAnswer.txt file")
    else:
        print('Error')

def deleteQA():
    question = input('Enter Question to delete: ')
    res = check_QA("questionAnswer.txt",question)
    if res == False:
        print("No such question found")
        return
    else:
        delete_QA("questionAnswer.txt",question)
        print('question deleted successfully and stored in questionAnswer.txt file')

def readQA():
    subject = input('Enter Subject: ')
    topic = input('Enter Topic: ')
    read_QA("questionAnswer.txt",subject, topic)

def update_credentials():
    usernameInput = input("Enter you current username : ")
    oldUsername = validateCharacter(usernameInput, 4, "old username")
    newUsernameInput = input("Set new username: ")
    newUsername = validateCharacter(newUsernameInput, 4, "new username")
    passwordInput = input("Set new password: ")
    password = validateCharacter(passwordInput, 6, "password")
    update_lecturer('lecturer.txt',oldUsername, newUsername, password)
    print("Lecturer credentials updated successfully and stored in lecturer.txt file")




